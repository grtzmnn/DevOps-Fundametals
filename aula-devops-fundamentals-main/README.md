# Aula DevOps Fundamentals

Projeto da aula DevOps Fundamentals - 2023

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.
Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

Entrega do projeto
    1.	Criar uma conta no GitLab
    2.	Criar o repositório
    3.	Baixar o projeto Aula Devops Fundamentals – link acima
    4.	Copiar ao seu repositório ou fazer Fork do projeto original
    5.	Fazer alterações no projeto conforme mencionado na aula
    6.	Fazer commit em seu repositório
    7.	Enviar a url do projeto para validarmos
    8.	Comandos básicos para auxiliar:
    9.	Após instalar o GitBash:
    10.	git clone NOME DO SEU REPOSITORIO HTTPS
 
Acessar o codigo:
    1.	Acessar o repositorio baixado: cd aula-devops-fundamentals
    2.	Fazer alteração, incluindo seu nome
    3.  Para ver as alterações feitas: git status
    4.  Adicionar suas alterações: git add . 
    5.  Fazer o Primeiro Commit: git commit -m "Primeiro Commit"
    6.	Para enviar suas alterações ao seu repositorio: git push origin main 



## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sandro.lechner/aula-devops-fundamentals.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/sandro.lechner/aula-devops-fundamentals/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

